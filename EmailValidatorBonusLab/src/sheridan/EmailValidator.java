package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
	
	public static void main(String[] args) {

	}
	
	public static boolean isValidEmail(String email) {
		
		Pattern pattern = Pattern.compile("[a-z]{1}[a-z0-9]{2,}@([a-zA-Z\\.])?[a-z0-9]{3,}\\.[a-zA-Z]{2,}", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		boolean matchFound = matcher.find();
		if(matchFound) {
			return true;
	    }
		else
		{
			return false;
		}
	}
}
