package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {
	
	@Test
	public void testIsEmailValid( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAccountRegular( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAccountBoundaryIn( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("luc@hotmail.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAccountBoundaryOut( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lu@hotmail.com");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAccountExceptional( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("#$%@hotmail.com");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidDomainRegular( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidDomainBoundaryIn( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hot.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidDomainBoundaryOut( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@ho.com");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidDomainExceptional( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@$%^&.com");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidExtensionRegular( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidExtensionBoundaryIn( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.co");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidExtensionBoundaryOut( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.c");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidExtensionExceptional( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.#$%^");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAtSymbolRegular( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("lucas@hotmail.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAtSymbolBoundaryIn( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("luc@hot.com");
		assertTrue("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAtSymbolBoundaryOut( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("luc@@hot.com");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
	@Test
	public void testIsEmailValidAtSymbolExceptional( ) {
		boolean isEmailValid = EmailValidator.isValidEmail("luc@@@@@hot.com");
		assertFalse("Unable to validate email.", isEmailValid);
	}
	
}
